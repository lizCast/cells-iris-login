{
  const {
    html,
  } = Polymer;
  /**
    `<cells-iris-login>` Description.

    Example:

    ```html
    <cells-iris-login></cells-iris-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-iris-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsIrisLogin extends Polymer.Element {

    static get is() {
      return 'cells-iris-login';
    }

    static get properties() {
      return {
        user: {
          type: String,
          value:''
        },
        pass: {
          type: String,
          value: ''
        },
        prop1: {
          type: Boolean,
          value: "false",
          notify:true
        }
      };
    }
    validaus(event){
      event.preventDefault();

      var usuario = event.target.user.value;
      var pass = event.target.pass.value;

      if(usuario != 'liz'){
        alert("Usuario o contraseña incorrectos");
        event.target.user.setAttribute("class", "error");
        return false;
      }
      else{
        event.target.user.setAttribute("class", "ok");
      }
      if(usuario == '' && pass == ''){
        alert("Usuario y contraseña necesarios");
        event.target.user.setAttribute("class", "error");
        event.target.pass.setAttribute("class", "error");
        return false;
      }
      if(pass.length < 6){
        alert("contraseña muy corta");
        event.target.pass.setAttribute("class", "error");
        return false;
      }
      if(pass.length > 8){
        alert("contraseña muy grande");
        event.target.pass.setAttribute("class", "error");
        return false;
      }
      else{
        alert("Sesión iniciada");
        event.target.user.setAttribute("class", "ok");
        event.target.pass.setAttribute("class", "ok");
        this.set('prop1',true);
      }
    }



    static get template() {
      return html `
      <style include="cells-iris-login-styles cells-iris-login-shared-styles"></style>
      <form action="#" method="POST" id="myForm" name="myForm" on-submit="validaus" on-prop1-changed> 
        <div class=" card ">
          <h1>Inicia sesión</h1>
          <div class="txt">
            <span>Usuario:</span>
          </div>
          <div>
            <input type="text" name="user" id="user" maxlength="10" placeholder="Inserta tu usuario">
          </div>
          <div class="txt">
            <span>Contraseña:</span>
          </div>
          <div class="marg-16">
            <input type="password" name="pass" id="pass" maxlength="8" placeholder="Inserta tu contraseña">
          </div>
          <div class="marg-16">
            <input type="submit" class="btn" value="Iniciar sesión"></input>
          </div>
        </div>
      </form>
      `;
    }
  }

  customElements.define(CellsIrisLogin.is, CellsIrisLogin);
}